<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.list-inline {
	list-style: none;
	padding: 0;
}

.list-inline>li {
	display: inline;
}
</style>
</head>
<body>

	<div>

		<h1>Page ListerNote</h1>

		<ul class="list-inline">
			<li><a href="index"> <em> Accueil</em></a></li>
			<li><a href="addNote"><em>AjouterNote</em></a></li>
			<li><a href="ListerNote"><em>ListerNote</em></a></li>
			<li><a href="rechercherNote"><em>RechercherNote</em></a></li>
			<li><a href="Quitter"><em> Quitter</em> </a></li>


		</ul>
	</div>
	<br>
	<form action="rechercherByCritere" method="Get">
		<fieldset id="sec-note">
			<legend>rechercherNoteparnote </legend>
			<div>
				<label for="uname">note:</label> <input min="1" max="5"
					type="number" placeholder="note" name="note" required>
			</div>
			<br>
			<button type="submit">Rechercher</button>
		</fieldset>
	</form>

	<form action="rechercherByCritere" method="Get">
		<fieldset id="sec-note">
			<legend>rechercherNoteparavis </legend>
			<div>
				<label for="uname">param:</label> <input type="text"
					placeholder="param"  name="mot" required>
			</div>
			<br> <br>
			<button type="submit">Rechercher</button>
		</fieldset>
	</form>

	<form action="rechercherByCritere" method="Get">
		<fieldset id="sec-note">
			<legend>rechercherNotepardateNote </legend>
			<div>
				<label for="uname">datNote:</label><input type="date"
					placeholder=" dateNote"  name="date" required>
			</div>
			<br>
			<button type="submit">Rechercher</button>
		</fieldset>
	</form>
	<br>
	<table border="1">

		<colgroup width="210" span="4">
		</colgroup>
		<tr>
			<th>note</th>
			<th>avis</th>
			<th>dateNote</th>
			<th>Supprimer</th>
			<th>Modifier</th>
		</tr>
		<c:forEach items="${listeNotes}" var="no">
			<tr>
				<td><c:out value="${no.note}" /></td>
				<td><c:out value="${no.avis}" /></td> 
				<td><c:out value="${no.dateNote}" /></td> 
				<td><a href="supprimerNote?id=<c:out value="${no.id}"/>">Supprimer</a></td>
				<td><a href="modifierNote?id=<c:out value="${no.id}"/>">Modifier</a></td>
			</tr>
		</c:forEach>
	</table>

</body>
</html>