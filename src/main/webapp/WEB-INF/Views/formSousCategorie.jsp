<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Formulaire sous Categorie</title>
</head>
<body>
	<form action="addSousCategorie" method="post">
		<select name="categorie">
			<c:forEach items="${listeCategories}" var="categorie" >
				<option value="${categorie.codeCategorie}"><c:out value="${categorie.intitule}"/></option>
			</c:forEach>
		</select>
        
        <p>intitule : <input type="text" name="intitule"/></p>
        <p>code addCategorie <input type="text" name="codeSousCategorie"/></p>
        <button type="submit">Valider</button>
    </form>
</body>
</html>