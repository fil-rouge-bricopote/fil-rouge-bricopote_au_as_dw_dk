<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h1>Liste des Categories</h1>
	
	<select name="categories">
		<c:forEach items="${listeCategories}" var="categorie" >
			<option value="${categorie.codeCategorie}"><c:out value="${categorie.intitule}"/></option>
		</c:forEach>
	</select>
	<ul>
		
	</ul>
	<a href="addCategorie">Ajouter une categorie</a>
	<a href="addSousCategorie">Ajouter une sous categorie</a>
</body>
</html>