<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:set var="contextPath" value="${pageContext.request.contextPath }"></c:set>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style type="text/css">
.list-inline {
	list-style: none;
	padding: 0;
}

.list-inline>li {
	display: inline;
}
</style>
</head>
<body>

	<div>

		<h1>
			Recherche Note par
			<c:out value="${titre }"></c:out>
		</h1>

		<ul class="list-inline">
			<li><a href="index"> <em> Accueil</em></a></li>
			<li><a href="addNote"><em>AjouterNote</em></a></li>
			<li><a href="ListerNote"><em>ListerNote</em></a></li>
			<li><a href="rechercherNote"><em>RechercherNote</em></a></li>
			<li><a href="Quitter"><em> Quitter</em> </a></li>

		</ul>
	</div>
	<br>
	<table border="1">

		<colgroup width="210" span="4">
		</colgroup>
		<tr>
			<th>note</th>
			<th>avis</th>
			<th>dateNote</th>
		</tr>
		<c:forEach items="${listeNotes}" var="no">
			<tr>
				<td><c:out value="${no.note}" /></td>
				<td><c:out value="${no.avis}" /></td>
				<td><c:out value="${no.dateNote}" /></td>
			</tr>
		</c:forEach>
	</table>
	<a href="${contextPath}/ListerNote"><em>Retour � la page de
			recherche </em> </a>
</body>
</html>