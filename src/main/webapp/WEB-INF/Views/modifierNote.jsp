<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<h1>
		<Strong> Page AjouterNote</Strong>
	</h1>

	<div>
		<ul>
			<li><a href="Quitter"><em> Quitter</em> </a></li>
			<li><a href="ListerNote"><em>ListerNote</em></a></li>
			<li><a href="rechercherNote"><em>RechercherNote</em></a></li>
			<li><a href="addNote"><em>AjouterNote</em></a></li>
			<li><a href="index"> <em> Accueil</em></a></li>
		</ul>
	</div>
	<br>

	<form action="updateNote" method="post">
		<fieldset id="sec-note">
			<legend> Modification de votre avis</legend>
			<div>

				<label for="uname">note:</label> <input min="1" max="5"
					type="number" placeholder="note" value="${note.note}" name="note"
					required>
			</div>
			<br>
			<div>
				<label for="uname">avis: </label> <input type="text"
					placeholder="avis" value="${note.avis }" name="avis" required>
			</div>
			<br>


			<div>
				<input type="hidden" placeholder="id" value="${note.id}" name="id"
					required>
			</div>
			<br>
			<button type="submit">Valider</button>
		</fieldset>

	</form>
</body>
</html>