package fr.afpa.bricopote.security;

import com.fasterxml.jackson.annotation.JsonView;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.beans.Views;
import fr.afpa.bricopote.services.UtilisateurServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@CrossOrigin(exposedHeaders = "authorization")
@RequestMapping("/login")
public class LoginController {

    private UtilisateurServiceImpl utilisateurService;

    @Autowired
    public LoginController(UtilisateurServiceImpl utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @JsonView(Views.public_user.class)
    @PostMapping("")
    public Utilisateur login(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if(principal != null){
            Long id = ((UserDetailsImpl)principal).getId();
            Optional<Utilisateur> user = utilisateurService.findByIdUtilisateur(id);
            return user.orElse(null);
        }
        return null;
    }
}
