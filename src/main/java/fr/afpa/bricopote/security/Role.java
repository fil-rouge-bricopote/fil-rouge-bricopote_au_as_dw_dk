package fr.afpa.bricopote.security;

public enum Role {
    ROLE_ADMIN, ROLE_BRICOLEUR, ROLE_USER;
}
