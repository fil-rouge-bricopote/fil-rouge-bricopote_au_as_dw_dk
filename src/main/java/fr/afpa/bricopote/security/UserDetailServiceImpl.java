package fr.afpa.bricopote.security;

import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.repositories.dao.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

    @Autowired
    private AccountRepository accountRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Account> account = accountRepository.findByEmail(username);
        account.orElseThrow(() -> new UsernameNotFoundException("Not found "+ username));
        return account.map(UserDetailsImpl::new).get();
    }
}
