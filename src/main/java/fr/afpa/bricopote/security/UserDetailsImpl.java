package fr.afpa.bricopote.security;

import fr.afpa.bricopote.beans.Account;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class UserDetailsImpl implements UserDetails {

    private Long id;
    private String email;
    private String password;
    private boolean active;
    private List<GrantedAuthority>  roles;

    public UserDetailsImpl(Account account) {
        this.id = account.getUser().getIdUser();
        this.email = account.getEmail();
        this.password = account.getPwd();
        this.roles = Arrays.asList(new SimpleGrantedAuthority(account.getUser().getRoles().toString()));
        this.active = account.isActive();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return roles;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
