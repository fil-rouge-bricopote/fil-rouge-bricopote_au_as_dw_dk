package fr.afpa.bricopote.repositories.dao;

import fr.afpa.bricopote.beans.Annonce;
import fr.afpa.bricopote.beans.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnonceRepository extends JpaRepository<Annonce, Long> {

    /**
     *
     * @param idAnnonce à rechercher
     * @return l'annonce si trouvé ou NULL
     */
    Annonce findByIdAnnonce(Long idAnnonce);

    /**
     *
     * @param utilisateur à recupperer ses annonces
     * @return la liste des annonces d'un utilisateur donné
     */
    List<Annonce> findByUtilisateur(Utilisateur utilisateur);
}
