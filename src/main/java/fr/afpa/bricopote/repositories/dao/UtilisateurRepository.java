package fr.afpa.bricopote.repositories.dao;

import java.util.List;
import java.util.Optional;

import fr.afpa.bricopote.security.Role;
import fr.afpa.bricopote.beans.Utilisateur;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UtilisateurRepository extends JpaRepository<Utilisateur, Long> {

    Optional<Utilisateur> findByIdUser(Long id);

    List<Utilisateur> findByNom(String nom);

    List<Utilisateur> findByRoles(Role role);

    @Query(value = "select u from Utilisateur u where lower(u.adresse.ville) = lower(:ville)")
    List<Utilisateur> findByVille(@Param("ville") String ville);

    Utilisateur findByEmail(String email);

    @Query("select s.bricoleurs from SousCategorie s where s.codeSousCategorie = :code")
    List<Utilisateur> findByCompetences(@Param("code") String code);

    @Query(
            value =
                    "select u from Utilisateur u " +
                            "where u.roles = 'ROLE_BRICOLEUR' " +
                            "and " +
                            "lower(concat(u.nom,' ',u.prenom,' ',u.nom,' ',u.tel,' ',u.email)) like concat('%',lower(trim(:key)),'%') ")
    List<Utilisateur> findBricoleurByKeyWord(@Param("key") String keyWord);


    @Query(value = "select distinct u from Utilisateur u join u.competences as s " +
            "where " +
            "( s.categorie.codeCategorie like concat('%',trim(:cat),'%') or s.codeSousCategorie like concat('%',trim(:cat),'%') ) " +
            " and " +
            "u.roles = 'ROLE_BRICOLEUR' " +
            "and " +
            "lower(concat(u.nom,' ',u.prenom,' ',u.nom,' ',u.tel,' ',u.email)) like concat('%',lower(trim(:key)),'%') " +
            "and " +
            "( lower(u.adresse.ville) like concat('%',lower(trim(:ville)),'%') or u.adresse.cp like trim(:ville))" +
            "and " +
            "(select a.active from Account a where a.id = u.idUser) = 'true' ")
    List<Utilisateur> findBricoleur(@Param("key") String keyWord, @Param("cat") String cat, @Param("ville") String ville , Pageable pageable);

    @Query(value = "select count(distinct u) from Utilisateur u join u.competences as s " +
            "where " +
            "( s.categorie.codeCategorie like concat('%',trim(:cat),'%') or s.codeSousCategorie like concat('%',trim(:cat),'%') ) " +
            " and " +
            "u.roles = 'ROLE_BRICOLEUR' " +
            "and " +
            "lower(concat(u.nom,' ',u.prenom,' ',u.nom,' ',u.tel,' ',u.email)) like concat('%',lower(trim(:key)),'%') " +
            "and " +
            "( lower(u.adresse.ville) like concat('%',lower(trim(:ville)),'%') or u.adresse.cp like trim(:ville))" +
            "and " +
            "(select a.active from Account a where a.id = u.idUser) = 'true' ")
    int getResultsCount(@Param("key") String keyWord,@Param("cat") String cat , @Param("ville") String ville);
}
