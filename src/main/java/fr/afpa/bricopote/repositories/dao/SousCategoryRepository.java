package fr.afpa.bricopote.repositories.dao;

import java.util.List;

import fr.afpa.bricopote.beans.Utilisateur;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.bricopote.beans.Categorie;
import fr.afpa.bricopote.beans.SousCategorie;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface SousCategoryRepository extends JpaRepository<SousCategorie, Long>{

	@Query(value = "select sc , b.idUser from SousCategorie sc join sc.bricoleurs as b where b.idUser = :id")
	List<SousCategorie> findCompetencesByBricoleur(@Param("id") Long idUser);

	List<SousCategorie> findByCategorie(Categorie categorie);

	List<SousCategorie> findByCodeSousCategorie(String codeSousCategorie);

}
