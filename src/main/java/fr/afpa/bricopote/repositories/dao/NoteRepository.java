package fr.afpa.bricopote.repositories.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import fr.afpa.bricopote.beans.Note;

public interface NoteRepository extends JpaRepository<Note, Long> {

	public List<Note> findByNoteAndDateNote(Integer note, String dateNote);

	@Query("SELECT  n FROM Note n WHERE n.avis LIKE %:motCle%")
	List<Note> rechercherByMotCle(@Param("motCle") String motCle);

	@Query("SELECT  n FROM Note n WHERE  n.note=:note")
	List<Note> rechercherNoteByNote(@Param("note") Integer note);

	@Query("SELECT  n FROM Note n WHERE  n.dateNote =:dateNote")
	List<Note> rechercherNoteByDateNote(@Param("dateNote") String dateNote);
}
