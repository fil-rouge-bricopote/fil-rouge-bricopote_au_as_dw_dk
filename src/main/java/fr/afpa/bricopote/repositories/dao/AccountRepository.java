package fr.afpa.bricopote.repositories.dao;

import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.beans.Utilisateur;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Account findByEmailAndAndPwd(String email , String Pwd);

    Account findByUser(Utilisateur user);

    Optional<Account> findByEmail(String email);

}
