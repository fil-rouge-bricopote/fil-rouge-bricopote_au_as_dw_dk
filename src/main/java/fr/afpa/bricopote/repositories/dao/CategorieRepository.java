package fr.afpa.bricopote.repositories.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.afpa.bricopote.beans.Categorie;

public interface CategorieRepository extends JpaRepository<Categorie, Long> {
	

	List<Categorie> findByCodeCategorie(String codeCategorie);
}
