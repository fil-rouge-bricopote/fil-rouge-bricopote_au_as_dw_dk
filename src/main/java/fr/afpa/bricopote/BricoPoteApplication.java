package fr.afpa.bricopote;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BricoPoteApplication {

    public static void main(String[] args) {
        SpringApplication.run(BricoPoteApplication.class, args);
    }

}
