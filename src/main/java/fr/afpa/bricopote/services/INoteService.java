package fr.afpa.bricopote.services;

import java.util.List;

import fr.afpa.bricopote.beans.Note;

public interface INoteService {
	void ajouterNote(Note no);

	void rechercherNotebyNoteAnddateNote(Integer note, String dateNote);

	List<Note> listeNote();

	List<Note> rechercherNote(Integer note, String dateNote);

	public Note rechercherNoteById(final Long Id);

	void supprimerNoteById(final Long id);

	void updateNoteById(final Long id, final String avis, final String dateNote);

	List<Note> rechercherNoteByMotCle(final String mot);

	List<Note> rechercherNoteByNote(Integer note);
	

	List<Note> rechercherNoteByDateNote( final String date);

}
