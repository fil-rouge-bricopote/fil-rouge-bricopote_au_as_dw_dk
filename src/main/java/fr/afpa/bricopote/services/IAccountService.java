package fr.afpa.bricopote.services;

import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.beans.Utilisateur;

public interface IAccountService {

    void saveAccount(Account account);

    void updateAccount(Account account);

    void deleteAccount(Account account);

    Account findByEmailAndAndPwd(String email , String pwd);

    Account findByUser(Utilisateur user);
}
