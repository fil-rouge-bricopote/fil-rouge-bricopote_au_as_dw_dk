package fr.afpa.bricopote.services;

import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.repositories.dao.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements IAccountService {

    @Autowired
    AccountRepository accountRepository;

    @Override
    public void saveAccount(Account account) {
        accountRepository.save(account);
    }

    @Override
    public void updateAccount(Account account) {
        accountRepository.saveAndFlush(account);
    }

    @Override
    public void deleteAccount(Account account) {
        accountRepository.delete(account);
    }

    @Override
    public Account findByEmailAndAndPwd(String email, String pwd) {
        return accountRepository.findByEmailAndAndPwd(email, pwd);
    }

    @Override
    public Account findByUser(Utilisateur user) {
        return accountRepository.findByUser(user);
    }
}
