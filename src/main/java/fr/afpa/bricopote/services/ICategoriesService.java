package fr.afpa.bricopote.services;

import java.util.List;

import fr.afpa.bricopote.beans.Categorie;
import fr.afpa.bricopote.beans.SousCategorie;

public interface ICategoriesService {

	List<Categorie> listerCategories();
	
	void ajouterCategorie(Categorie categorie);

}
