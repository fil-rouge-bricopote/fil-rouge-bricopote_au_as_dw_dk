package fr.afpa.bricopote.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.bricopote.beans.Categorie;
import fr.afpa.bricopote.beans.SousCategorie;
import fr.afpa.bricopote.repositories.dao.CategorieRepository;

@Service
public class CategoriesServiceIMP implements ICategoriesService{
	
	@Autowired
	CategorieRepository catRepo;

	@Override
	public List<Categorie> listerCategories() {
		
		return catRepo.findAll();
	}

	@Override
	public void ajouterCategorie(Categorie categorie) {
		catRepo.save(categorie);
	}

}
