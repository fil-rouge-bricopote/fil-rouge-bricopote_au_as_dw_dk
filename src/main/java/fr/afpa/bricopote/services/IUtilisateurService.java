package fr.afpa.bricopote.services;

import fr.afpa.bricopote.security.Role;
import fr.afpa.bricopote.beans.Utilisateur;
import org.apache.catalina.User;

import java.util.List;
import java.util.Optional;

public interface IUtilisateurService {

    Optional<Utilisateur> findByIdUtilisateur(Long user_id);

    List<Utilisateur> findByNom(String nom);

    List<Utilisateur> findByRoles(Role role);

    List<Utilisateur> findByVille(String ville);

    Utilisateur findByEmail(String email);

    List<Utilisateur> findByCompetences(String code);

    List<Utilisateur> findAll();

    List<Utilisateur> findBricoleurByKeyWord(String keyWord);

    List<Utilisateur> findBricoleur(String keyWord, String cat, String ville, int page, int nbResults);

    int getResultsCount(String keyWord, String cat, String ville);

    void updateUtilisateur(Utilisateur user);
}
