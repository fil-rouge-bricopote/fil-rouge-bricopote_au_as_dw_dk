package fr.afpa.bricopote.services;

import fr.afpa.bricopote.security.Role;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.repositories.dao.UtilisateurRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UtilisateurServiceImpl implements IUtilisateurService {

    @Autowired
    private UtilisateurRepository utilisateurRepository;

    @Override
    public Optional<Utilisateur> findByIdUtilisateur(Long user_id) {

        return utilisateurRepository.findByIdUser(user_id);

    }

    @Override
    public List<Utilisateur> findByNom(String nom) {
        return utilisateurRepository.findByNom(nom);
    }

    @Override
    public List<Utilisateur> findByRoles(Role role) {
        return utilisateurRepository.findByRoles(role);
    }

    @Override
    public List<Utilisateur> findByVille(String ville) {
        return utilisateurRepository.findByVille(ville);
    }

    @Override
    public Utilisateur findByEmail(String email) {
        return utilisateurRepository.findByEmail(email);
    }

    @Override
    public List<Utilisateur> findByCompetences(String code) {
        return utilisateurRepository.findByCompetences(code);
    }

    @Override
    public List<Utilisateur> findAll() {
        return utilisateurRepository.findAll();
    }

    @Override
    public List<Utilisateur> findBricoleurByKeyWord(String keyWord) {
        return utilisateurRepository.findBricoleurByKeyWord(keyWord);
    }

    @Override
    public List<Utilisateur> findBricoleur(String keyWord,String cat,String ville,int page,int nbResults) {
        return utilisateurRepository.findBricoleur(keyWord,cat,ville, PageRequest.of(page,nbResults, Sort.Direction.ASC,"nom"));
    }

    @Override
    public int getResultsCount(String keyWord, String cat, String ville) {
        return utilisateurRepository.getResultsCount(keyWord,cat,ville);
    }

    @Override
    public void updateUtilisateur(Utilisateur user) {
         utilisateurRepository.saveAndFlush(user);
    }
}
