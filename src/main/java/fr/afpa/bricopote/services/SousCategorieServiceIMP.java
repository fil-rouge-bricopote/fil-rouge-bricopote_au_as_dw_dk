package fr.afpa.bricopote.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.bricopote.beans.Categorie;
import fr.afpa.bricopote.beans.SousCategorie;
import fr.afpa.bricopote.repositories.dao.CategorieRepository;
import fr.afpa.bricopote.repositories.dao.SousCategoryRepository;

@Service
public class SousCategorieServiceIMP implements ISousCategoriesService {
	
	@Autowired
	SousCategoryRepository sousCatRepo;
	
	@Autowired
	CategorieRepository catRepo;

	@Override
	public List<SousCategorie> listerSousCategories() {

		return sousCatRepo.findAll();
	}

	@Override
	public void ajouterSousCategorie(String codeCat, SousCategorie sousCategorie) {
		List<Categorie> listCategorie = catRepo.findByCodeCategorie(codeCat);
		Categorie categorie = listCategorie.get(0);
		List<SousCategorie> listSousCategories = sousCatRepo.findByCategorie(categorie);
		sousCategorie.setCategorie(categorie);
		listSousCategories.add(sousCategorie);
		categorie.setListSousCategories(listSousCategories);
		catRepo.save(categorie);
	}

	@Override
	public List<SousCategorie> listerSousCategoriesByCat(String codeCat) {
		List<Categorie> listCategorie = catRepo.findByCodeCategorie(codeCat);
		if(listCategorie.size() > 0) {
			Categorie categorie = listCategorie.get(0);
			return sousCatRepo.findByCategorie(categorie);
		}
		return null;
	}

	@Override
	public List<SousCategorie> listerCompetencesByBricoleur(Long id) {
		return sousCatRepo.findCompetencesByBricoleur(id);
	}

	@Override
	public SousCategorie findSousCategorieByCodeSousCat(String codeSCat) {
		List<SousCategorie> listSousCat = sousCatRepo.findByCodeSousCategorie(codeSCat);
		for (SousCategorie scat: listSousCat){
			return scat;
		}
		return null;
	}

}
