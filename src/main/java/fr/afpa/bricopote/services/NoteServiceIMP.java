package fr.afpa.bricopote.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import fr.afpa.bricopote.beans.Note;
import fr.afpa.bricopote.repositories.dao.NoteRepository;

@Service
public class NoteServiceIMP implements INoteService {

	@Autowired
	NoteRepository noteRepository;

	@Override
	public void ajouterNote(Note no) {
		noteRepository.save(no);

	}

	@Override
	public void rechercherNotebyNoteAnddateNote(Integer note, String dateNote) {
		noteRepository.findByNoteAndDateNote(note, dateNote);

	}

	@Override
	public List<Note> listeNote() {
		return noteRepository.findAll();
	}

	@Override
	public List<Note> rechercherNote(Integer note, String dateNote) {
		noteRepository.findByNoteAndDateNote(note, dateNote);
		return noteRepository.findByNoteAndDateNote(note, dateNote);
	}

	@Override
	public Note rechercherNoteById(final Long Id) {

		return noteRepository.findById(Id).get();
	}

	@Override
	public void supprimerNoteById(Long id) {

		noteRepository.deleteById(id);
	}

	@Override
	public void updateNoteById(final Long id, final String avis, final String dateNote) {
		final Note noteDo = rechercherNoteById(id);
		noteDo.setAvis(avis);
		noteDo.setDateNote(dateNote);
		noteRepository.save(noteDo);

	}

	@Override
	public List<Note> rechercherNoteByMotCle(final String mot) {

		return noteRepository.rechercherByMotCle(mot);
	}

	@Override
	public List<Note> rechercherNoteByNote(final Integer note) {
		return noteRepository.rechercherNoteByNote(note);
	}

	@Override
	public List<Note> rechercherNoteByDateNote(final String dateNote) {
		return noteRepository.rechercherNoteByDateNote(dateNote);
	}

}
