package fr.afpa.bricopote.services;

import fr.afpa.bricopote.beans.Annonce;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.repositories.dao.AnnonceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AnnonceServiceImpl implements IAnnonceService{

    private AnnonceRepository annonceRepository;

    /**
     *
     * @param annonceRepository la dependence à injecter
     */
    @Autowired
    public AnnonceServiceImpl(AnnonceRepository annonceRepository){
        this.annonceRepository = annonceRepository;
    }

    /**
     *
     * @param annonce l'annonce à enregistrer dans la base de données
     */
    @Override
    public void saveAnnonce(Annonce annonce){
        annonceRepository.save(annonce);
    }

    /**
     *
     * @param annonce
     */
    @Override
    public void updateAnnonce(Annonce annonce){
        annonceRepository.saveAndFlush(annonce);
    }

    /**
     *
     * @param Annonce à supprimer
     */
    @Override
    public void deleteAnnonce(Annonce annonce) {
        annonceRepository.delete(annonce);
    }

    /**
     *
     * @param idAnnonce à trouver
     * @return
     */
    @Override
    public Annonce findByIdAnnonce(Long idAnnonce) {
        return annonceRepository.findByIdAnnonce(idAnnonce);
    }

    /**
     *
     * @param utilisateur à recupperer ses annonces
     * @return
     */
    @Override
    public List<Annonce> findByUtilisateur(Utilisateur utilisateur) {
        return annonceRepository.findByUtilisateur(utilisateur);
    }
}
