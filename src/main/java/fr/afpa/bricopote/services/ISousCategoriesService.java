package fr.afpa.bricopote.services;

import java.util.List;

import fr.afpa.bricopote.beans.SousCategorie;

public interface ISousCategoriesService{

	List<SousCategorie> listerSousCategories();

	void ajouterSousCategorie(String codeCat, SousCategorie sousCategorie);

	List<SousCategorie> listerSousCategoriesByCat(String codeCat);

    List<SousCategorie> listerCompetencesByBricoleur(Long id);

    SousCategorie findSousCategorieByCodeSousCat(String codeSCat);
}
