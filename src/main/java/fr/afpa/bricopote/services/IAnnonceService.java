package fr.afpa.bricopote.services;

import fr.afpa.bricopote.beans.Annonce;
import fr.afpa.bricopote.beans.Utilisateur;

import java.util.List;

public interface IAnnonceService {
        /**
         *
         * @param annonce à enregistrer
         */
        void saveAnnonce(Annonce annonce);

        /**
         *
         * @param annonce à mettre à jour
         */
        void updateAnnonce(Annonce annonce);

        /**
         *
         * @param annonce à supprimer
         */
        void deleteAnnonce(Annonce annonce);

        /**
         *
         * @param idAnnonce à trouver
         * @return l'annonce si trouvé ou NULL
         */
        Annonce findByIdAnnonce(Long idAnnonce);

        /**
         *
         * @param utilisateur à recupperer ses annonces
         * @return la liste des annonces dun utilisateur donné
         */
        List<Annonce> findByUtilisateur(Utilisateur utilisateur);

}
