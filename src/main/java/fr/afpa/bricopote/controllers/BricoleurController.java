package fr.afpa.bricopote.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.beans.Views;
import fr.afpa.bricopote.services.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class BricoleurController {

    private IUtilisateurService utilisateurService;

    @Autowired
    public BricoleurController(IUtilisateurService utilisateurService) {
        this.utilisateurService = utilisateurService;
    }

    @JsonView(Views.public_user.class)
    @GetMapping("public/bricoleur/search/{key}/")
    public List<Utilisateur> findUBricoleurByKeyWord(@PathVariable("key") String keyWord) {
        return utilisateurService.findBricoleurByKeyWord(keyWord);
    }

    @JsonView(Views.public_user.class)
    @GetMapping("public/bricoleur/search/{key}/{cat}/{ville}/{page}/{nbResults}/")
    public List<Utilisateur> findBricoleur(
            @PathVariable("key") String keyWord,
            @PathVariable("cat") String cat,
            @PathVariable("ville") String ville,
            @PathVariable("page") int page,
            @PathVariable("nbResults") int nbResults) {
        return utilisateurService.findBricoleur(keyWord, cat, ville, page, nbResults);
    }

    @JsonView(Views.public_user.class)
    @GetMapping("public/bricoleur/search")
    public List<Utilisateur> findBricoleur2(
            @RequestParam("key") String keyWord,
            @RequestParam("cat") String cat,
            @RequestParam("ville") String ville,
            @RequestParam("page") int page,
            @RequestParam("nbResults") int nbResults) {
        System.out.println(cat + " " + keyWord + " " + ville);
        return utilisateurService.findBricoleur(keyWord, cat, ville, page, nbResults);
    }

    @GetMapping("public/bricoleur/nb")
    public int getResultsCount(
            @RequestParam("key") String keyWord,
            @RequestParam("cat") String cat,
            @RequestParam("ville") String ville) {
        return utilisateurService.getResultsCount(keyWord, cat, ville);
    }

    @GetMapping("public/bricoleur/search-id/{id}")
    public Utilisateur findByRoles(@PathVariable("id") Long id) {
        Utilisateur res = null;
        return utilisateurService.findByIdUtilisateur(id).orElse(null);
    }
}
