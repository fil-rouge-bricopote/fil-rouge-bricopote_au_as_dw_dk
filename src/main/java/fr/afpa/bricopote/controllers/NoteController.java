package fr.afpa.bricopote.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import fr.afpa.bricopote.services.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.bricopote.beans.Note;
import fr.afpa.bricopote.services.INoteService;

@Controller
public class NoteController {
	@Autowired
	INoteService noteService;


	// la page d'accueil
	@GetMapping(value = "/index")
	public String redirectFormAccueil() {
		return "index";

	}

	@GetMapping(value = "/addNote")
	public String redirectForm() {
		return "ajouterNote";
	}

	@PostMapping(value = "/addNote")
	public ModelAndView ajouterNote(ModelAndView mv, @RequestParam(value = "note") Integer note,
			@RequestParam(value = "avis") String avis) {
		Note no = new Note();
		no.setNote(note);
		no.setAvis(avis);
		Date dateNote = new Date();
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		no.setDateNote(formater.format(dateNote));
		noteService.ajouterNote(no);
		List<Note> liste = noteService.listeNote();
		mv.addObject("listeNotes", liste);
		mv.setViewName("listeNote");
		return mv;


//@RequestParam(value = "dateNote") String datNote
	@GetMapping(value = "/ListerNote")
	public ModelAndView listeNote(ModelAndView mv) {
		List<Note> liste = noteService.listeNote();
		mv.addObject("listeNotes", liste);
		mv.setViewName("listeNote");

		return mv;

	}

	@GetMapping(value = "/rechercherNote")
	public String rechercherNoteRedirect() {
		return "rechercherNote";

	}

	@PostMapping(value = "/rechercherNote")
	public ModelAndView rechercherNote(ModelAndView mv, @RequestParam(value = "note") Integer note,
			@RequestParam(value = "dateNote") String dateNote) {
		String[] elements = dateNote.split("-");
		String dateFormater = elements[2] + "/" + elements[1] + "/" + elements[0];
		List<Note> notes = noteService.rechercherNote(note, dateFormater);
		mv.addObject("listeNotes", notes);
		mv.setViewName("listeNote");
		return mv;
	}
// rechercher par mot clé

	@GetMapping(value = "/rechercherByCritere")
	public ModelAndView rechercherByCritere(ModelAndView mv,
			@RequestParam(value = "note", defaultValue = "0") Integer note,
			@RequestParam(value = "mot", defaultValue = "") String mot,
			@RequestParam(value = "date", defaultValue = "01/01/1900") String date) {

		if (!mot.isEmpty()) {
			List<Note> liste = noteService.rechercherNoteByMotCle(mot);
			mv.addObject("listeNotes", liste);
			mv.addObject("titre", "mot clé");
			mv.setViewName("rechercherByCritere");
		}
		if (note != 0) {
			List<Note> liste = noteService.rechercherNoteByNote(note);
			mv.addObject("listeNotes", liste);
			mv.addObject("titre", "note");
			mv.setViewName("rechercherByCritere");
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		if (!date.equals("01/01/1900")) {
			String[] elements = date.split("-");
			String dateFormater = elements[2] + "/" + elements[1] + "/" + elements[0];
			List<Note> liste = noteService.rechercherNoteByDateNote(dateFormater);
			mv.addObject("titre", "date");
			mv.addObject("listeNotes", liste);
			mv.setViewName("rechercherByCritere");
		}

		return mv;
	}

// j' acceder à mon formulaire
	@GetMapping(value = "/modifierNote")
	public ModelAndView rechercherNoteById(ModelAndView mv, @RequestParam(value = "id") Long id) {
		Note no = noteService.rechercherNoteById(id);
		mv.addObject("note", no);
		mv.setViewName("modifierNote");
		return mv;
	}

	// suppression d'une note
	@GetMapping(value = "/supprimerNote")
	public String supprimerNoteById(@RequestParam(value = "id") Long id) {
		noteService.supprimerNoteById(id);
		return "redirect:ListerNote";
	}

	// modifier la note puis j'enregistre
	@PostMapping(value = "/updateNote")
	public ModelAndView updateNote(ModelAndView mv, @RequestParam(value = "avis") String avis,
			@RequestParam(value = "id") Long id) {
		Date dateNote = new Date();
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");

		noteService.updateNoteById(id, avis, formater.format(dateNote));
		List<Note> liste = noteService.listeNote();
		mv.addObject("listeNotes", liste);
		mv.setViewName("listeNote");
		return mv;
	}

	// quitter et rediriger vers la page d'accueil
	@GetMapping(value = "/Quitter")
	public String redirectFormQuitter() {
		return "index";

	}

}