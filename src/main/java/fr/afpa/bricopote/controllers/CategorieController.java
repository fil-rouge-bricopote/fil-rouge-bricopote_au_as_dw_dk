package fr.afpa.bricopote.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import fr.afpa.bricopote.beans.Categorie;
import fr.afpa.bricopote.beans.SousCategorie;
import fr.afpa.bricopote.services.ICategoriesService;
import fr.afpa.bricopote.services.ISousCategoriesService;

@CrossOrigin
@RestController
@RequestMapping("api/public")
public class CategorieController {
	
	@Autowired
	ICategoriesService catServ;
	
	@Autowired
	ISousCategoriesService sousCatServ;


	@GetMapping("/categories")
	public List<Categorie> allCategories() {
		
		List<Categorie> listCat = catServ.listerCategories();
		return listCat;
	}

	@GetMapping("/souscategories")
	public List<SousCategorie> allSousCategories() {

		List<SousCategorie> listSousCat = sousCatServ.listerSousCategories();
		return listSousCat;
	}

	@GetMapping("/souscategories/categorie/{codeCat}")
	public List<SousCategorie> listSousCategoriesByCat(@PathVariable String codeCat) {

		List<SousCategorie> listSousCat = sousCatServ.listerSousCategoriesByCat(codeCat);
		return listSousCat;
	}

	@GetMapping("/souscategories/bricoleur/{id}")
	public List<SousCategorie> listSousCategoriesByCat(@PathVariable Long id) {

		List<SousCategorie> listSousCat = sousCatServ.listerCompetencesByBricoleur(id);
		return listSousCat;
	}

	@GetMapping("/souscategorie/{codeSCat}")
	public SousCategorie findSousCategorieByCodeSCat(@PathVariable String codeSCat){

		SousCategorie sousCat = sousCatServ.findSousCategorieByCodeSousCat(codeSCat);
		return sousCat;
	}
	
	/*@GetMapping("/addCategorie")
	public String addCategorie() {
		return "formCategorie";
	}
	
	@GetMapping(value = "/addSousCategorie")
	public ModelAndView addSousCategorie(ModelAndView mv) {
		List<Categorie> listCat = catServ.listerCategories();
		mv.addObject("listeCategories", listCat);
		mv.setViewName("formSousCategorie");
		return mv;
	}
	
	@PostMapping("/addCategorie")
	public ModelAndView addCategorie(ModelAndView mv, @RequestParam(value = "intitule") String intitule , @RequestParam(value = "codeCategorie") String codeCatogorie ) {
		Categorie categorie = new Categorie(intitule, codeCatogorie);
		catServ.ajouterCategorie(categorie);
		mv.addObject("listeCategories", catServ.listerCategories());
		mv.setViewName("listeCategorie");
		return mv;
	}
	
	@PostMapping("/addSousCategorie")
	public ModelAndView addSousCategorie(ModelAndView mv, @RequestParam(value = "categorie") String codeCat , @RequestParam(value = "intitule") String intitule , @RequestParam(value = "codeSousCategorie") String codeSousCategorie ) {
		SousCategorie sousCategorie = new SousCategorie(intitule, codeSousCategorie);
		sousCatServ.ajouterSousCategorie(codeCat ,sousCategorie);
		mv.addObject("listeCategories", catServ.listerCategories());
		mv.setViewName("listeCategorie");
		return mv;
	}*/
	
}
