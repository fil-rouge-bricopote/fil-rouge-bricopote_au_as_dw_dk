package fr.afpa.bricopote.controllers;

import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.security.Role;
import fr.afpa.bricopote.services.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.time.LocalDate;

@RestController
@CrossOrigin
@RequestMapping("/api/")
public class AuthController {

    @Autowired
    IAccountService accServ;

    @Autowired
    UtilisateurController utilisateurController;

    @PostMapping(value = "public/user/inscription")
    public Account addAccountUser(@RequestBody Account account){


        account.getUser().setRoles(Role.ROLE_USER);
        account.getUser().setCreateDate(LocalDate.now());
        account.getUser().setLastUpdate(LocalDate.now());
        account.setActive(true);

        accServ.saveAccount(account);

        return account;
    }


    @PostMapping(value = "public/bricopote/inscription")
    public Account addAccountBricopote(@RequestBody Account account){
        account.setActive(true);
        account.getUser().setCreateDate(LocalDate.now());;
        account.getUser().setRoles(Role.ROLE_BRICOLEUR);
        account.getUser().setLastUpdate(LocalDate.now());
        accServ.saveAccount(account);

        return account;
    }

    @DeleteMapping("user/auto-desactivate")
    public boolean autoDesactivate(){
        Utilisateur user = utilisateurController.getInfo();
        Account account = accServ.findByUser(user);
        if(account!=null){
            account.setActive(false);
            accServ.updateAccount(account);
            return true;
        }
        return false;
    }


    @RolesAllowed("ADMIN")
    @DeleteMapping("/desactivate/{id}")
    public void desactivateUser(@PathVariable("id") Long id){

    }
}
