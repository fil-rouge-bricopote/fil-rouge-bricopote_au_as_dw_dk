package fr.afpa.bricopote.controllers;

import fr.afpa.bricopote.beans.Annonce;
import fr.afpa.bricopote.services.IAnnonceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("api/public/")
public class AnnonceController {

    @Autowired
    private IAnnonceService annonceServ;

    @PostMapping(value = "annonce/add")
    public void addAnnonce(@RequestBody Annonce annonce){
        annonceServ.saveAnnonce(annonce);
    }

}
