package fr.afpa.bricopote.controllers;

import com.fasterxml.jackson.annotation.JsonView;
import fr.afpa.bricopote.beans.Account;
import fr.afpa.bricopote.beans.Views;
import fr.afpa.bricopote.security.Role;
import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.security.UserDetailsImpl;
import fr.afpa.bricopote.services.IAccountService;
import fr.afpa.bricopote.services.IUtilisateurService;
import lombok.var;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/api/user")
public class UtilisateurController {

    private IUtilisateurService utilisateurService;

    @Autowired
    private IAccountService accServ;

    @Autowired
    public UtilisateurController(IUtilisateurService utilisateurService){
        this.utilisateurService = utilisateurService;
    }

    @JsonView(Views.privateLiteUser.class)
    @GetMapping("/info")
    public Utilisateur getInfo(){
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        System.out.println(principal);
        if(principal != null){
            Long id = ((UserDetailsImpl)principal).getId();
            Optional<Utilisateur> user = utilisateurService.findByIdUtilisateur(id);
            return user.orElse(null);
        }
        return null;
    }

    @GetMapping("/info/role")
    public String getRole(){
        var authority = SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        if(authority != null && !authority.isEmpty()){
            return authority.stream().collect(Collectors.toList()).get(0).getAuthority();
        }
        return "";
    }

    @GetMapping("search-nom/{nom}")
    public List<Utilisateur> findByNom(@PathVariable("nom") String nom){
        List<Utilisateur> res = Arrays.asList();
        if(!"".equals(nom)){
            res = utilisateurService.findByNom(nom);
        }
        return res;
    }

    @GetMapping("search-ville/{ville}")
    public List<Utilisateur> findByVille(@PathVariable("ville") String ville){
        List<Utilisateur> res = Arrays.asList();
        if(!"".equals(ville)){
            res = utilisateurService.findByVille(ville);
        }
        return res;
    }

    @GetMapping("search-email/{email}")
    public Utilisateur findByEmail(@PathVariable("email") String email){
        Utilisateur res = null;
        if(!"".equals(email)){
            res = utilisateurService.findByEmail(email);
        }
        return res;
    }

    @GetMapping("search-role/{role}")
    public List<Utilisateur> findByRoles(@PathVariable("role") Role role){
        List<Utilisateur> res = Arrays.asList();
        if(role != null){
            res = utilisateurService.findByRoles(role);
        }
        return res;
    }

    @GetMapping("search-Competence/{code}")
    public List<Utilisateur> findByRoles(@PathVariable("code") String code){
        List<Utilisateur> res = Arrays.asList();
        if(!"".equals(code)){
            res = utilisateurService.findByCompetences(code);
        }
        return res;
    }

    //@JsonView(Views.public_user.class)
    @RolesAllowed("ADMIN")
    @GetMapping("/")
    public List<Utilisateur> findAll(){
        return utilisateurService.findAll();
    }

    @PutMapping("/update")
    public Utilisateur updateUtilisateur(@RequestBody Utilisateur user){


        user.setLastUpdate(LocalDate.now());

        Account account = accServ.findByUser(user);
        account.setEmail(user.getEmail());
        account.setUser(user);
        accServ.updateAccount(account);

        return user;
    }

}
