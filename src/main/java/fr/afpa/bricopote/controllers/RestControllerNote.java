package fr.afpa.bricopote.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import fr.afpa.bricopote.beans.Utilisateur;
import fr.afpa.bricopote.services.IUtilisateurService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import fr.afpa.bricopote.beans.Note;
import fr.afpa.bricopote.services.INoteService;

// autorise le port 3000 
@CrossOrigin(origins = "http://localhost:3000")
@RestController

@RequestMapping("notes")
public class RestControllerNote {
	@Autowired
	INoteService noteService;

	@Autowired
	IUtilisateurService utilisateurService;

	@GetMapping("/liste")
	public List<Note> getListeNotes() {
		return noteService.listeNote();
	}

// ajout d'une note 
	@PostMapping(value = "/addNote")
	public void ajouterNote(@RequestBody Note note) {
		System.out.println(note);
		Note no = new Note();
		no.setNote(note.getNote());
		no.setAvis(note.getAvis());
		//Optional<Utilisateur> user = utilisateurService.findByIdUtilisateur(idBricoleur);
		//System.out.println("=================================================================>" + user);
		//no.setUtilisateur(user.get());
		Date dateNote = new Date();
		SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
		no.setDateNote(formater.format(dateNote));
		noteService.ajouterNote(no);

	}

}
