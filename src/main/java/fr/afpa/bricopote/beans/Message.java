package fr.afpa.bricopote.beans;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "Message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int idMessage;
	@NonNull
	private LocalDateTime date_heure;
	@NonNull
	private String contenu;
	@ManyToMany
	@JoinTable(name = "Utilisateur_Message_Association", joinColumns = @JoinColumn(name = "idMessage"), inverseJoinColumns = @JoinColumn(name = "idUtilisateur"))
	private List<Utilisateur> utilisateurs = new ArrayList<Utilisateur>();

}
