package fr.afpa.bricopote.beans;

import com.fasterxml.jackson.annotation.JsonView;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@ToString
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Adresse {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "add_gen")
    @SequenceGenerator(name = "add_gen",sequenceName = "seq_adresse",allocationSize = 1)
    @Column(name = "adresse_id")
    @JsonView(Views.privateLiteUser.class)
    private Long idAdresse;


    @NonNull
    @NotBlank(message = "Le numero est requis.")
    @JsonView(Views.privateLiteUser.class)
    private String numero;


    @NonNull
    @NotBlank(message = "Le libéllé est requis.")
    @JsonView(Views.privateLiteUser.class)
    private String libelle;


    @NonNull
    @NotBlank(message = "Le code postal est requis.")
    @JsonView(Views.public_user.class)
    private String cp;

    @NotBlank(message = "La ville est requise.")
    @NonNull
    @JsonView(Views.public_user.class)
    private String ville;
}
