package fr.afpa.bricopote.beans;

import javax.persistence.CascadeType;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import javax.persistence.Table;

import org.springframework.lang.NonNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity @Table(name = "Note")
public class Note {
@Id
@GeneratedValue(strategy = GenerationType.AUTO)
 private Long id;
@NonNull
 private int note;
@NonNull
 private String avis;
@NonNull
 private  String dateNote;
@ManyToOne( cascade = CascadeType.ALL )
@JoinColumn( name="idUtilisateur") 
private Utilisateur utilisateur;
}
