package fr.afpa.bricopote.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class Categorie {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id_categorie")
	private Long idCategorie;
	@NonNull
	private String intitule;
	@NonNull
	@Column(name = "code_categorie")
	private String codeCategorie;

	//@JsonIgnore
	@OneToMany(mappedBy = "categorie" , fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<SousCategorie> listSousCategories;

}
