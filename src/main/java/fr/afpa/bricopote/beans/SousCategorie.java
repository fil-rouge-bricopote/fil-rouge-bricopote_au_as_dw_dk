package fr.afpa.bricopote.beans;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@RequiredArgsConstructor
@NoArgsConstructor
@Entity
public class SousCategorie {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_sous_categorie")
    private Long idSousCategorie;
    @NonNull
    private String intitule;
    @NonNull
    private String codeSousCategorie;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnoreProperties(value = {"listSousCategories", "handler","hibernateLazyInitializer"}, allowSetters = true)
    @JoinColumn(name = "id_categorie")
    private Categorie categorie;

    @JsonIgnore
    @ManyToMany()
    @JoinTable(name = "Bricoleur_Competence",
            joinColumns = @JoinColumn(name = "id_sous_categorie"),
            inverseJoinColumns = @JoinColumn(name = "id_user"))
    private List<Utilisateur> bricoleurs;

    @JsonIgnore
    @OneToMany(mappedBy = "sousCategorie")
    private List<Annonce> annonces;


}
