package fr.afpa.bricopote.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import fr.afpa.bricopote.security.Role;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Getter
@Setter
@ToString
public class Utilisateur {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "utilisateur_gen")
    @SequenceGenerator(name = "utilisateur_gen",sequenceName = "utilisateur_seq",allocationSize = 1)
    @Column(name = "id_user")
    @JsonView(Views.public_user.class)
    private Long idUser;

    @NonNull
    @JsonView(Views.public_user.class)
    private String nom;

    @NonNull
    @JsonView(Views.public_user.class)
    private String prenom;

    @Pattern(regexp = "0[1-9][0-9]{8}", message = "Le numéro de tel doit être correct")
    @NonNull
    @JsonView(Views.public_user.class)
    private String tel;

    @NonNull
    @Column(unique = true)
    @Email(message = "L'Email doit être valide ! ")
    @JsonView(Views.public_user.class)
    private String email;

    @NonNull
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "Date_Naissance")
    @JsonView(Views.privateLiteUser.class)
    private LocalDate dateNaissance;

    @Column(nullable = true)
    @JsonView(Views.public_user.class)
    private String imageProfil;

    @Column(nullable = true)
    @JsonView(Views.public_user.class)
    private Boolean smallImage;

    @OneToOne(cascade = {CascadeType.MERGE, CascadeType.REFRESH})
    @JoinColumn(name = "adresse_id", referencedColumnName = "adresse_id" )
    @JsonView(Views.public_user.class)
    private Adresse adresse;

    @JsonIgnore
    @OneToMany(mappedBy = "utilisateur",cascade = CascadeType.ALL)
    private List<Annonce> annonces;


    @ManyToMany()
    @JoinTable( name = "Bricoleur_Competence",
            joinColumns = @JoinColumn(name = "id_user"),
            inverseJoinColumns =  @JoinColumn( name = "id_sous_categorie"))
    private List<SousCategorie> competences;


    @JsonView(Views.privateLiteUser.class)
    @Enumerated(EnumType.STRING)
    private Role roles;


    @JsonView(Views.public_user.class)
    @Column(name = "Date_Creation")
    private LocalDate createDate;

    @Column(name = "Date_Modification")
    private LocalDate lastUpdate;

}
