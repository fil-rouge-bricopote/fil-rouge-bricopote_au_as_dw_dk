package fr.afpa.bricopote.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@RequiredArgsConstructor
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
public class Annonce {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "ann_gen")
    @SequenceGenerator(name = "ann_gen",sequenceName = "annonce_seq",allocationSize = 1)
    @Column(name = "annonce_id")
    private Long idAnnonce;

    @NotBlank(message = "le titre est requis")
    @Size(max = 255 ,message = "Il ne faut pas dépasser les 255 chars.")
    @NonNull
    private String titre;

    @NotBlank(message = "Il faut mettre de contenu.")
    @NonNull
    private String contenu;

    @NonNull
    @Column(name = "date_annonce")
    private LocalDate dateAnnonce;

    @Min(value = 0,message = "le tarif minimal est 0.")
    @NonNull
    private float tarif;

    @NonNull
    @NotBlank(message = "La localisation est requise.")
    private String localisation;

    @NonNull
    @Column(name = "nb_vues")
    private int nbVues;

    @NonNull
    @ManyToOne()
    @JoinColumn(name = "id_categorie",referencedColumnName = "id_sous_categorie")
    private SousCategorie sousCategorie;

    @ManyToOne()
    @JsonIgnore
    @JoinColumn(name = "id_utilisateur" , referencedColumnName = "id_user")
    private Utilisateur utilisateur;
}
