package fr.afpa.bricopote.beans;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.lang.NonNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@AllArgsConstructor
@RequiredArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
public class Blog {
@Id
	@GeneratedValue(strategy =GenerationType.AUTO )
	private int idAtricle;
	@NonNull
	private String titre;
	@NonNull
	private String contenu;
	@NonNull
	private String auteur;
	@NonNull
	private String dateArticle;
}
